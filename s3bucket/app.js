process.env.NODE_ENV = 'localDevelopment';
express = require('express');
path = require('path');
bodyParser = require('body-parser');
http = require('http');
app = express();

var multipart = require('connect-multiparty');
var routes = require('./routes/index');
var users = require('./routes/users');
config = require('config');
var connection = require('./routes/mySqlLib');
var multipartMiddleware = multipart();

sendResponse = require('./routes/sendResponse');
func = require('./routes/commonFunction');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', routes);
app.post('/user_image', multipartMiddleware);
app.post('/user_image', users);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});

module.exports = app;
