var fs = require('fs');
var AWS = require('aws-sdk');


exports.checkBlankWithCallback = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {

        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}

function checkBlank(arr) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] == '' || arr[i] == undefined || arr[i] == '(null)') {
            return 1;
            break;
        }
    }
    return 0;
}
exports.checkEmailAvailability = function (res, email, callback) {

    var sql = "SELECT `user_id` FROM `tb_user` WHERE `user_email`=? limit 1";
    connection.query(sql, [email], function (err, response) {

        if (err) {

            console.log(err);
            sendResponse.somethingWentWrongError(res);
        }
        else if (response.length) {
            callback(null);
        }
        else {
            sendResponse.emailNotRegistered(res);
        }
    });
};
exports.uploadImageToS3Bucket = function (file, folder, callback) {
    var filename = file.name;
    var path = file.path;
    var mimeType = file.type;

    fs.readFile(path, function (error, file_buffer) {
        if (error) {
            sendResponse.somethingWentWrongError(res);
        }
        filename = file.name;
        finalpath = config.get("s3").Url + filename;
        srcpath = config.get("s3").Url;
        if (finalpath == srcpath) {
            sendResponse.emptyFileUpload(res);
        }
        else {
            AWS.config.update({accessKeyId: config.get("s3").AccessKey, secretAccessKey: config.get("s3").SecretKey});
            var s3bucket = new AWS.S3();
            var params = {
                Bucket: config.get("s3").userName,
                Key: folder + '/' + filename,
                Body: file_buffer,
                ACL: 'public-read',
                ContentType: 'image/jpeg'
            };
            console.log(params);
            s3bucket.putObject(params, function (err, data) {
                if (err) {
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    return callback(finalpath);
                }
            });
        }
    });
};

