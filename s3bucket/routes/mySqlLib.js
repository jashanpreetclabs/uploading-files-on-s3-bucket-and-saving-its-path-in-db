/**
 * Created by JASHAN on 3/9/2015.
 */
var mysql = require('mysql');

connection = mysql.createConnection({
    host: config.get("DATABASE").host,
    user: config.get("DATABASE").user,
    database: config.get("DATABASE").database
});
connection.connect();