var constant = require('./constant');

exports.somethingWentWrongError = function (res) {

    var errResponse = {
        status: constant.responseStatus.ERROR_IN_EXECUTION,
        message: constant.responseMessage.ERROR_IN_EXECUTION,
        data: {}
    }
    sendData(errResponse, res);
};

exports.parameterMissingError = function (res) {

    var errResponse = {
        status: constant.responseStatus.PARAMETER_MISSING,
        message: constant.responseMessage.PARAMETER_MISSING,
        data: {}
    }
    sendData(errResponse, res);
};

exports.emailNotRegistered = function (res) {

    var errResponse = {
        status: constant.responseStatus.EMAIL_NOT_REGISTERED,
        message: constant.responseMessage.EMAIL_NOT_REGISTERED,
        data: {}
    }
    sendData(errResponse, res);
};

exports.sendSuccessData = function (data, res) {

    var successResponse = {
        status: constant.responseStatus.SHOW_DATA,
        message: constant.responseMessage.SUCCESSFUL_EXECUTION,
        data: data
    };
    sendData(successResponse, res);
};
exports.emptyFileUpload = function (res) {

    var errResponse = {
        status: constant.responseStatus.EMPTY_FILE,
        message: constant.responseMessage.EMPTY_FILE,
        data: {}
    };
    sendData(errResponse, res);
};

function sendData(data, res) {
    res.type('json');
    res.json(data);
}