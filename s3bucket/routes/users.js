var async = require('async');
var router = express.Router();

router.post('/user_image', function (req, res) {

    var email = req.body.email;
    async.waterfall(
        [function (callback) {
            func.checkBlankWithCallback(res, [email], callback);
        },
            function (callback) {
                func.checkEmailAvailability(res, email, callback);

            }], function (updatePopup) {
            func.uploadImageToS3Bucket(req.files.user_image, 'jashan', function (result) {
                var sql = "UPDATE tb_user SET image=? WHERE `user_email`=?";
                connection.query(sql, [result, email], function (err, result) {

                    if (err) {
                        sendResponse.somethingWentWrongError(res);
                    }

                    else {
                        var sql = "SELECT `image`FROM `tb_user` WHERE `user_email`=? LIMIT 1"
                        connection.query(sql, [email], function (err, user) {
                            if (err) {
                                sendResponse.somethingWentWrongError(res);
                            }
                            else {
                                var final = {"user_image": user[0].image, "email": email};
                                sendResponse.sendSuccessData(final, res);
                            }

                        });
                    }

                });

            });
        });

});
module.exports = router;

